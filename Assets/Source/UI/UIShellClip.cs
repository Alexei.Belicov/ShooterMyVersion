﻿using UnityEngine;

public class UIShellClip : MonoBehaviour
{
    [SerializeField] Transform rootXF;
    private int displayedCount;

    public int DisplayedCount
    {
        get => displayedCount;
        set
        {
            Debug.Assert(value >= 0);
            displayedCount = value;
            ShowShells(displayedCount);
        }
    }

    private void ShowShells(int count)
    {
        int childCount = rootXF.childCount;
        Debug.Assert(count <= childCount);

        for (int i = 0; i < childCount; ++i)
            rootXF.GetChild(i).gameObject.SetActive(i < count);
    }
}
