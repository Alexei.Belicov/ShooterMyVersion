﻿using UnityEngine.UI;
using UnityEngine;

public class BulletsUI : MonoBehaviour
{
    [SerializeField] WeaponBase weapon;
    [SerializeField] Text bulletsText;

    void Update()
    {
        bulletsText.text = weapon.GetBullets().ToString();

        if (weapon.GetBullets().ToString() == "0")
            bulletsText.text = "<color=#CD1900>R</color>";
    }
}