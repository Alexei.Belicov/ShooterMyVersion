﻿using UnityEngine;
using UnityEngine.UI;

[ExecuteAlways]
public class UIProgressBar : MonoBehaviour
{
    const float BAR_HOR_PADDING = 2f;

    [SerializeField] RectTransform healthBarRXF;
    [SerializeField] RectTransform referenceRect;
    [Range(0f, 1f), SerializeField] float value;

    private Vector2 barSizeDeltaMax, barSizeDeltaMin;

    public float Value
    {
        get => value;
        set
        {
            this.value = Mathf.Clamp01(value);
            healthBarRXF.sizeDelta = Vector2.Lerp(barSizeDeltaMin, barSizeDeltaMax, this.value);
        }
    }

    void OnEnable()
    {
        Vector2 rectSize = referenceRect.sizeDelta;
        Vector2 barSize = healthBarRXF.sizeDelta;

        barSizeDeltaMax.y = -BAR_HOR_PADDING * 2f;
        barSizeDeltaMax.x = -BAR_HOR_PADDING * 2f;

        barSizeDeltaMin = barSizeDeltaMax;
        barSizeDeltaMin.x = -rectSize.x;
    }

    void OnValidate()
    {
        this.Value = this.value;
    }
}