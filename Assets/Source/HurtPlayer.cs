﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayer : MonoBehaviour
{
    public int damageToGive;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != PlayerController.TAG)
            return;

        collision.gameObject.GetComponent<PlayerHealthManager>().HurtPlayer(damageToGive);
        //enemyAnimator.Play("Attack01", 0, 0);
        //audioSource.PlayOneShot(attackSound);
    }
}
