﻿using System.Collections;
using UnityEngine;

public class PortalController : MonoBehaviour
{
    [SerializeField] PortalNode portalA, portalB;
    [SerializeField] Color color;

    [System.Obsolete]
    void Awake()
    {
        portalA.SetColor(color);
        portalB.SetColor(color);

        portalA.IsActive = true;
        portalB.IsActive = true;
    }

    public void TeleportObject(Rigidbody who, PortalNode source)
    {
        PortalNode destination = source == portalA ? portalB : portalA;
        destination.IsActive = false;
        StartCoroutine(TeleportObjectCR(who, destination));
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.2f, 0.4f, 0.9f);
        Gizmos.DrawLine(portalA.transform.position, portalB.transform.position);
    }

    IEnumerator TeleportObjectCR(Rigidbody rb, PortalNode destination)
    {
        rb.gameObject.SetActive(false);
        portalA.IsActive = portalB.IsActive = false;
        yield return new WaitForSeconds(1f);
        rb.gameObject.SetActive(true);
        rb.position = destination.transform.position;
        PortalNode source = destination == portalA ? portalB : portalA;
        source.IsActive = true;
    }
}