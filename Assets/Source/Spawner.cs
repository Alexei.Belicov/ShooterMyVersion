﻿using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject enemyPrototype;
    [SerializeField] StartingPoints startingPoints;
    [SerializeField] SpawnerEnemyWave[] waves;

    public static int enemyCount;
    public GameObject spawnEffectPrefab;

    IEnumerator Start()
    {
        PlayerController playerController = GetPlayerDamageableInterface();

        for (int waveIdx = 0; waveIdx < waves.Length; waveIdx++)
        {

            while (Spawner.enemyCount > 0)
                yield return null;

            yield return new WaitForSeconds(waves[waveIdx].afterWaveDelay);

            for (int enemyIdx = 0; enemyIdx < waves[waveIdx].count; enemyIdx++)
            {
                Vector3 pos = startingPoints.GetRandomStartPosition();
                var go = Instantiate(enemyPrototype, pos, Quaternion.identity, transform);
                var enemyController = go.GetComponent<EnemyController>();
                Debug.Assert(enemyController != null);

                enemyController.OnDead += this.EnemyDeadHandler;
                playerController.OnDead += enemyController.PlayerGameOverHandler;
                playerController.OnDead += this.PlayerDeadHandler;

                enemyCount++;
                yield return new WaitForSeconds(waves[waveIdx].delay);
            }
        }
    }

    private void EnemyDeadHandler()
    {
        Debug.Assert(enemyCount > 0);
        enemyCount--;
    }

    private void PlayerDeadHandler()
    {
        this.StopAllCoroutines();
    }

    private PlayerController GetPlayerDamageableInterface()
    {
        GameObject playerGameObject = GameObject.FindGameObjectWithTag(PlayerController.TAG);
        Debug.Assert(playerGameObject != null, "Can't find player object");
        var c = playerGameObject.GetComponent<PlayerController>();
        Debug.Assert(c != null);
        return c;
    }
}

[System.Serializable]
public struct SpawnerEnemyWave
{
    public float delay;
    public float afterWaveDelay;
    public int count;
}