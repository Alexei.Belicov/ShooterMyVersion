﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthManager : MonoBehaviour
{
    public float playerMaxHealth = 100f;
    public float playerCurrentHealth;

    public float HealthPoints
    {
        get { return playerCurrentHealth; }
        set
        {
            playerCurrentHealth = Mathf.Clamp(value, 0f, 100f); // HP points range

            if (playerCurrentHealth <= 0f)
            {
                //Dead
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        SetMaxHealth();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerCurrentHealth <= 0f)
        {
            gameObject.SetActive(false);
        }
    }

    public void HurtPlayer(float damageToGive)
    {
        playerCurrentHealth -= damageToGive;
    }

    public void SetMaxHealth()
    {
        playerCurrentHealth = playerMaxHealth;
    }
}
