﻿using UnityEngine;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] Color color;
    [SerializeField] Renderer materialDestination;
    [SerializeField] Transform pivotXF;

    Camera myCamera;
    Transform cameraXF;

    void Awake()
    {
        materialDestination.material.color = color;  // clone/replace
        cameraXF = Camera.main.transform;
    }

    void LateUpdate()
    {
        transform.LookAt(transform.position + cameraXF.rotation * Vector3.back, cameraXF.rotation * Vector3.up);
    }

    public float Progress
    {
        set
        {
            float f = Mathf.Clamp01(value);
            pivotXF.localScale = new Vector3(f, 1f, 1f);
        }
    }

    public bool Visible
    {
        get
        {
            return gameObject.activeSelf;
        }

        set
        {
            gameObject.SetActive(value);
        }
    }
}