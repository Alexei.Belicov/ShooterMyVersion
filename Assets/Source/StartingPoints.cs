﻿using System.Collections.Generic;
using UnityEngine;

public class StartingPoints : MonoBehaviour
{
    Transform[] allPoints;
    List<int> randomList = new List<int>();

    void Awake()
    {
        allPoints = new Transform[transform.childCount];
        for (int i = 0; i < allPoints.Length; i++)
            allPoints[i] = this.transform.GetChild(i);
    }

    public Vector3 GetRandomStartPosition()
    {
        if (randomList.Count == 0)
        {
            for (int i = 0; i < allPoints.Length; i++)
                randomList.Add(i);
            Shuffle(randomList);
        }

        int randomIndex = Random.Range(0, randomList.Count);
        int transfromIndex = randomList[randomIndex];
        randomList.RemoveAt(randomIndex);
        return allPoints[transfromIndex].position;
    }

    public static void Shuffle<T>(IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}