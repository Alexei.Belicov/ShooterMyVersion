﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    public GameObject pauseScreen;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PauseOn()
    {
        Time.timeScale = 0f; // Time.timeScale - время течения
        //player.enabled = false; // на всякий случай деактивируем компонент player
        pauseScreen.SetActive(true);
        //musicSource.volume = 0.1f; // приглушаем громкость музыки
    }
}
