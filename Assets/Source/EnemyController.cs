﻿using System;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(MeshRenderer))]
public class EnemyController : MonoBehaviour, IDamageable
{
    const float ATTACK_DISTANCE = 4f;

    [SerializeField] AudioSource audioSource;
    [SerializeField] ParticleSystem particleSys;
    [SerializeField] ProgressBar healthBar;
    [SerializeField] float autodestroyTime = 1f;
    [SerializeField] float deliverDamageAmount = 10f;
    [SerializeField] float health = 100f;
    [SerializeField] float pathfindingCountPerSecond = 10f;

    private Animator enemyAnimator;
    private NavMeshAgent agent;
    private Transform playerXF;
    private float nextPathfindingTime;
    private float currentHealth;
    private float defaultAgentSpeed;
    bool playerIsDead = false;

    private Rigidbody RBody
    {
        get
        {
            if (rbody_ == null)
            {
                rbody_ = this.GetComponent<Rigidbody>();
                Debug.Assert(rbody_ != null, "Component not found");
            }
            return rbody_;
        }
    }
    Rigidbody rbody_;

    public AudioClip attackSound, deathSound;
    public GameObject deathEffectPrefab;
    public float Health => currentHealth;
    public event Action OnDead;

    void Awake()
    {
        agent = this.GetComponent<NavMeshAgent>();
        enemyAnimator = this.GetComponent<Animator>();
        audioSource = this.GetComponent<AudioSource>();

        GameObject playerGameObject = GameObject.FindGameObjectWithTag(PlayerController.TAG);
        Debug.Assert(playerGameObject != null, "Can't find player object");
        playerXF = playerGameObject.transform;
        currentHealth = health;
        healthBar.Progress = 1f;
        defaultAgentSpeed = agent.speed;
    }

    void FixedUpdate()
    {
        float pathfindingDelay = 1f / pathfindingCountPerSecond;
        if (Time.time > nextPathfindingTime)
        {
            agent.SetDestination(playerXF.position);
            nextPathfindingTime = Time.time + pathfindingDelay;
        }

        if ((playerXF.position - transform.position).sqrMagnitude < ATTACK_DISTANCE * ATTACK_DISTANCE)
        {
            agent.speed = defaultAgentSpeed * 100f;
            RBody.AddForce(RBody.velocity.normalized * 20f);
        }
        else
        {
            // test for max speed
            if (RBody.velocity.magnitude > defaultAgentSpeed)
            {
                RBody.velocity = RBody.velocity.normalized * defaultAgentSpeed;
                agent.speed = defaultAgentSpeed;
            }
        }
    }

    public void PlayerGameOverHandler()
    {
        if(playerIsDead == true)
            return;

        this.enabled = false;
        this.agent.enabled = false;
        enemyAnimator.enabled = false;
        //enemyAnimator.Play("Idle", 0, 0);
    }

    public void TakeDamage(float damage, Vector3 direction)
    {
        if (currentHealth <= 0f) return;

        currentHealth -= damage;
        enemyAnimator.Play("GetHit", 0, 0);
        if (currentHealth <= 0f)
        {
            currentHealth = 0f;
            particleSys.transform.forward = direction;
            enemyAnimator.Play("Die", 0, 0);
            playerIsDead = true;
            Autodestroy();
        }

        healthBar.Progress = currentHealth / health;
    }

    private void Autodestroy()
    {
        this.enabled = false;
        this.agent.enabled = false;
        this.GetComponent<Collider>().enabled = false;
        this.GetComponent<Renderer>().enabled = false;
        //particleSys.Play();

        Destroy(this.gameObject, autodestroyTime);
        Quaternion quaternion = new Quaternion(1, 0, 0, 0);
        GameObject deathEffectClone = Instantiate(deathEffectPrefab, agent.transform.position, quaternion);
        Destroy(deathEffectClone, 2);

        healthBar.gameObject.SetActive(false);
        audioSource.PlayOneShot(deathSound);

        OnDead.Invoke();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != PlayerController.TAG)
            return;

        if (playerIsDead == false)
        {
            enemyAnimator.Play("Attack01", 0, 0);
            //audioSource.PlayOneShot(attackSound);

            var iDamageable = collision.gameObject.GetComponent<IDamageable>();
            Debug.Assert(iDamageable != null);
            Vector3 forceDirection = -collision.contacts[0].normal;
            iDamageable.TakeDamage(deliverDamageAmount, forceDirection);
        }
    }
}