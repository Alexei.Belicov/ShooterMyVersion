﻿using UnityEngine.Assertions;
using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody))]

public class PlayerController : MonoBehaviour, IDamageable
{
    public const string TAG = "Player";
    public const float PUSH_FORCE = 20f;  // TODO: extract into SO

    [SerializeField] AudioSource audioSource;
    [SerializeField] ProgressBar reloadBar;
    [SerializeField] Transform crosshairXF;
    [SerializeField] Transform weaponSlotXF;
    [SerializeField] float maxVelocity = 10f;
    [SerializeField] float minAimingDistance = 1f;
    [SerializeField] float moveForce;

    private Animator playerAnimator;
    private Rigidbody playerRigidBody;
    private UIProgressBar uiHealthBar;
    private float maxHealth;
    private float verticalAxis, horizontalAxis;

    public float Health { get; private set; } = 100f;  // TODO: extract into scriptable object

    public event Action OnDead;

    void Awake()
    {
        playerAnimator = this.GetComponent<Animator>();
        playerRigidBody = this.GetComponent<Rigidbody>();
        uiHealthBar = this.GetHealthProgressBar();
        reloadBar.Visible = false;
        maxHealth = Health;
    }

    void Start()
    {
        uiHealthBar.Value = 1f;
    }

    void Update()
    {
        WeaponBase myWeapon = GetComponentInChildren<WeaponBase>();
        if (myWeapon == null) return;

        if (Input.GetMouseButton(0))
            myWeapon.Fire();

        if (Input.GetKeyDown(KeyCode.R))
        {
            myWeapon.SetReloadBar(reloadBar);
            myWeapon.Reload();
        }

        AnimationState();
        AnimationWeapon();
    }

    void FixedUpdate()
    {
        PlayerMover();
        LookAtMousePosition();
    }

    public void TakeDamage(float damage, Vector3 direction)
    {
        if (Health == 0f) return;

        Assert.IsFalse(damage < 0f);
        Health = Mathf.Max(0f, Health - damage);

        uiHealthBar.Value = Health / maxHealth;
        playerRigidBody.AddForce(direction * PUSH_FORCE, ForceMode.Impulse);

        if (Health == 0f)
            PlayerGameOver();
    }

    private void AnimationState()
    {
        playerAnimator.SetFloat("DirectionZ", verticalAxis);
        playerAnimator.SetFloat("DirectionX", horizontalAxis);
    }

    private void AnimationWeapon()
    {
        if (Input.GetMouseButton(0))
        {
            if (WeaponBase.currBulletCount != 0)
                playerAnimator.SetBool("Attack", true);
        }
        else
            playerAnimator.SetBool("Attack", false);

        if (Input.GetKey(KeyCode.R))
        {
            if (WeaponBase.isInReload)
                playerAnimator.SetBool("Reload", true);
        }
        else
            playerAnimator.SetBool("Reload", false);
    }

    private void LookAtMousePosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        int raycastMask = LayerMask.GetMask("Ground", "InfiniteCollider");
        bool wasHit = Physics.Raycast(ray, out hitInfo, 1000f, raycastMask);
        if (!wasHit) return;

        //bool wasGroundHit = hitInfo.collider.gameObject.tag != "Infinity-collider";
        //Cursor.visible = !wasGroundHit;
        //crosshairXF.gameObject.SetActive(wasGroundHit);

        Vector3 hitPosition = hitInfo.point;
        Vector3 lookPosition = hitPosition;

        // rotate player to target
        lookPosition.y = this.transform.position.y;
        this.transform.LookAt(lookPosition);

        // rotate weapon slot to target
        Vector3 playerGroundPosition = this.transform.position;
        playerGroundPosition.y = hitPosition.y;

        if ((playerGroundPosition - hitPosition).magnitude > minAimingDistance)
        {
            lookPosition.y = weaponSlotXF.position.y;
            this.weaponSlotXF.LookAt(lookPosition);
        }

        // crosshair move
        crosshairXF.position = hitPosition;
    }

    private void PlayerGameOver()
    {
        this.enabled = false;
        playerAnimator.enabled = false;
        OnDead.Invoke();
    }

    private void PlayerMover()
    {
        if (playerRigidBody.velocity.sqrMagnitude < maxVelocity * maxVelocity)
        {
            verticalAxis = Input.GetAxis("Vertical");
            horizontalAxis = Input.GetAxis("Horizontal");
            playerRigidBody.AddForce(moveForce * horizontalAxis, 0f, moveForce * verticalAxis);
        }
    }

    private UIProgressBar GetHealthProgressBar()
    {
        var go = GameObject.FindGameObjectWithTag("canvas-ui"); // this is the canvas obj
        Debug.Assert(go != null);
        uiHealthBar = go.GetComponentInChildren<UIProgressBar>();
        Debug.Assert(uiHealthBar != null);
        Debug.Assert(go.GetComponentsInChildren<UIProgressBar>().Length == 1, "Multiple bars, please refactor this");
        return uiHealthBar;
    }
}