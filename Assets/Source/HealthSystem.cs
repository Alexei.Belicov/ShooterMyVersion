﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour
{
    public float startingHealth = 100f;
    private float _healthPoints = 100f;

    public float HealthPoints
    {
        get { return _healthPoints; }
        set
        {
            _healthPoints = Mathf.Clamp(value, 0f, 100f); // HP points range

            if (_healthPoints <= 0f)
            {
                //Dead
            }
        }
    }

    void Start() 
    {
        HealthPoints = startingHealth;
    }
}
