﻿using UnityEngine;
public class RandomizeIdleType : MonoBehaviour
{
    Animator idleAnimator;
    void Start()
    {
        idleAnimator = GetComponent<Animator>();
    }
    private void Awake() {
        RandomIdle();
    }
    void RandomIdle()
    {
        int pickAnumber = Random.Range(1, 3);//exclusive never prints the last only goes 1 to 2
        Debug.Log(pickAnumber);

        //randJumpInt is the parameter in animator
        //pickAnumber random number from 1 to 2 from above
        idleAnimator.SetInteger("IdleRandom", pickAnumber);

    
    }
}
