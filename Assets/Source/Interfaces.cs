﻿using System;
using UnityEngine;

public interface IDamageable
{
    float Health { get; }

    event Action OnDead;

    void TakeDamage(float damage, Vector3 direction);
}