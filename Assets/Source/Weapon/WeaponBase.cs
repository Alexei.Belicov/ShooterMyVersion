﻿using System.Collections;
using UnityEngine;

public abstract class WeaponBase : MonoBehaviour
{
    [SerializeField] ShellBase shell;
    [SerializeField] Transform muzzleXF;
    [SerializeField] int maxBullets = 10;
    [SerializeField] float fireCooldown;
    [SerializeField] float reloadTime = 1f;
    [SerializeField] AudioSource audioSource;
    public AudioClip shootingSound;
    public AudioClip reloadSound;
    //public AudioClip noBullets;
    public static int currBulletCount;
    public Transform MuzzleXF => muzzleXF;
    public static bool isInReload = false;
    ProgressBar reloadBar;
    float nextFireTime;

    void Awake()
    {
        currBulletCount = maxBullets;
    }

    public void Fire()
    {
        if (isInReload) return;
        if (currBulletCount <= 0) return;
        if (Time.time < nextFireTime) return;

        audioSource.PlayOneShot(shootingSound);

        Instantiate(shell, MuzzleXF.position, MuzzleXF.rotation);
        nextFireTime = Time.time + fireCooldown;
        currBulletCount--;
    }

    public void Reload()
    {
        if (currBulletCount == maxBullets)
            return;
            
        if (!reloadBar.Visible)
        {
            audioSource.PlayOneShot(reloadSound);
            StartCoroutine(ReloadRoutine());
        }
    }

    //HACK: should be done via events ReloadRoutine
    public void SetReloadBar(ProgressBar bar)
    {
        Debug.Assert(bar != null);
        reloadBar = bar;
    }

    IEnumerator ReloadRoutine()
    {
        Debug.Assert(reloadBar != null, "Progress bar should be set first by SetReloadBar()");
        float startTime = Time.time;
        reloadBar.Visible = true;
        currBulletCount = 0;
        isInReload = true;

        while (Time.time < startTime + reloadTime)
        {
            float elapsedTime = Time.time - startTime;
            reloadBar.Progress = elapsedTime / reloadTime;
            yield return null;
        }

        reloadBar.Progress = 1f;
        currBulletCount = maxBullets;
        yield return new WaitForSeconds(0.3f);
        isInReload = false;
        reloadBar.Visible = false;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(MuzzleXF.position, MuzzleXF.forward * 10f);

        if (this.transform.parent != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.parent.position, transform.parent.forward * 10f);
        }
    }

    public int GetBullets()
    {
        return currBulletCount;
    }
}