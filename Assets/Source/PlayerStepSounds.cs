﻿using UnityEngine;

public class PlayerStepSounds : MonoBehaviour
{
    [SerializeField] AudioClip[] currentStepSounds;
    private AudioSource audioSource;
    private int randomAudioClip, previousAudioClip;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayStepSounds()
    {
        randomAudioClip = Random.Range(0, currentStepSounds.Length);
        if (randomAudioClip == previousAudioClip) // если сгенерировалось значение, равное предыдущему
            randomAudioClip = randomAudioClip + 1;
        if (randomAudioClip == currentStepSounds.Length) // если вылезли за границы массива
            randomAudioClip = 0;
        previousAudioClip = randomAudioClip;
        audioSource.PlayOneShot(currentStepSounds[randomAudioClip], 0.3f);
    }
}