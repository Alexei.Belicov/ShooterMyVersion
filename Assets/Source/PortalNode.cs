﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PortalNode : MonoBehaviour
{
    [SerializeField] GameObject visualElement;
    [SerializeField] GameObject recolorElementsRoot;
    [SerializeField] ParticleSystem fxParticles;
    [SerializeField] float sleepTime;
    [SerializeField] float triggerDistance = 0.1f;


    private Transform triggerXF;

    public bool IsActive
    {
        get => _isActive_;
        set
        {
            _isActive_ = value;
            visualElement.SetActive(value);
        }
    }
    private bool _isActive_;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag != "Player")
            return;
        triggerXF = other.transform;
        if (!IsActive)
            return;

        float distance = (transform.position - other.transform.position).magnitude;
        if (distance < triggerDistance)
            TeleportObject(other.attachedRigidbody);
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag != "Player")
            return;
        IsActive = true;
        triggerXF = null;
    }

    [System.Obsolete]
    public void SetColor(Color color)
    {
        var allRenderers = recolorElementsRoot.GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < allRenderers.Length; i++)
            allRenderers[i].material.color = color;

        fxParticles.startColor = color;
    }


    private void TeleportObject(Rigidbody teleport)
    {
        IsActive = false;
        Debug.Assert(transform.parent != null, "Should have parent");
        var parentController = transform.parent.GetComponent<PortalController>();
        Debug.Assert(parentController != null, "Expected component not found");
        parentController.TeleportObject(teleport, this);
        fxParticles.Play();
    }

    void OnDrawGizmos()
    {
        if (triggerXF == null)
            return;
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, triggerXF.position);
    }
}